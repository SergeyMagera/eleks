﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Products.Common;
using Products.Common.Interfaces;

namespace Products.BLL.Interfaces
{
    public interface IProductsService
    {
        Task<List<IProduct>> GetProductsByFilterAsync(ProductsOptions options);
        Task<IProduct> GetProductByIdAsync(int id);
        Task AddAsync(IProduct product);
        Task EditAsync(int id, IProduct product);
        Task RemoveAsync(int id);
    }
}
