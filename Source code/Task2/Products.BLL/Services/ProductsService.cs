﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.WebApi.Exceptions;
using Products.BLL.Interfaces;
using Products.Common;
using Products.Common.Interfaces;
using Products.DAL.Interfaces;

namespace Products.BLL.Services
{
    public class ProductsService : IProductsService
    {
        private readonly IResponseErrorService _errorService;
        private readonly IUnitOfWork _db;

        public ProductsService(IResponseErrorService errorService, IUnitOfWork db)
        {
            _errorService = errorService;
            _db = db;
        }

        public Task<List<IProduct>> GetProductsByFilterAsync(ProductsOptions options)
        {
            return _db.Products.GetByFilterAsync(options);
        }

        public Task<IProduct> GetProductByIdAsync(int id)
        {
            var product = _db.Products.GetByIdAsync(id);
            if(product == null)
                throw new HttpStatusCodeException(_errorService.ProductNotFound());
            return product;
        }

        public async Task AddAsync(IProduct prod)
        {
            await _db.Products.AddAsync(prod);
            await _db.SaveAsync();
        }

        public async Task EditAsync(int id, IProduct product)
        {
            var prod = await _db.Products.GetByIdAsync(id);
            if(prod == null)
                throw new HttpStatusCodeException(_errorService.ProductNotFound());
            // Todo: use automapper
            
            await _db.Products.UpdateAsync(prod);
            await _db.SaveAsync();
        }

        public Task RemoveAsync(int id)
        {
           return _db.Products.RemoveAsync(id);
        }
    }
}
