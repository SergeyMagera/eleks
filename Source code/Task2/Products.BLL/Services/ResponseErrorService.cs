﻿using Base.WebApi.Services;
using Common.Library.Models;
using Products.Common.Interfaces;

namespace Products.BLL.Services
{
    public class ResponseErrorService : BaseResponseError, IResponseErrorService
    {
        public ResponseError ProductNotFound()
        {
            return new ResponseError("product_not_found", "Product not found");
        }
    }
}
