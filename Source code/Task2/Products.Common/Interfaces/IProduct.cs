﻿using Base.DAL.Interfaces;
using Common.Library.Interfaces;

namespace Products.Common.Interfaces
{
    public interface IProduct : IImage, IModelId<int>
    {
        string Name { get; set; }
        decimal Price { get; set; }
        int Amount { get; set; }
        string Description { get; set; }
    }
}