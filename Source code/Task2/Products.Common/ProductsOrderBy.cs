﻿namespace Products.Common
{
    public enum ProductsOrderBy
    {
        Name,
        Price,
        Amount
    }
}
