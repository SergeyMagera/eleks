﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Base.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Base.DAL.Repositories
{
    public abstract class BaseRepository<T, TId, TContext> : IBaseRepository<T, TId> where T : class, IModelId<TId> where TContext : DbContext
    {
        protected TContext Context { get; set; }

        protected BaseRepository(TContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task<T> GetByIdAsync(TId id)
        {
            return Context.Set<T>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public Task<List<T>> GetManyAsync(Expression<Func<T, T>> selector = null,
            Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            bool disableTracking = false, int skip = 0, int take = 20)
        {
            IQueryable<T> query = Context.Set<T>();
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }

            if (include != null)
            {
                query = include(query);
            }

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (selector != null)
            {
                query = query.Select(selector);
            }

            return query.ToListAsync();
        }

        public Task<T> GetFirstOrDefaultAsync(Expression<Func<T, T>> selector,
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            bool disableTracking = false)
        {
            IQueryable<T> query = Context.Set<T>();
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }

            if (include != null)
            {
                query = include(query);
            }

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.Select(selector).FirstOrDefaultAsync();
        }

        public Task<bool> AnyAsync(Expression<Func<T, bool>> predicate = null)
        {
            IQueryable<T> query = Context.Set<T>();
            return predicate != null ? query.AnyAsync(predicate) : query.AnyAsync();
        }

        public Task AddAsync(T entity)
        {
            return Context.Set<T>().AddAsync(entity);
        }

        public Task UpdateAsync(T entity)
        {
            Context.Set<T>().Update(entity);
            return Task.CompletedTask;
        }

        public async Task RemoveAsync(TId id)
        {
            var entity = await GetByIdAsync(id);
            Context.Set<T>().Remove(entity);
        }
    }
}