﻿using System;
using System.Threading.Tasks;

namespace Base.DAL.Interfaces
{
    public interface IBaseUnitOfWork : IDisposable
    {
        Task<int> SaveAsync();
        void Dispose(bool dispose);
    }
}