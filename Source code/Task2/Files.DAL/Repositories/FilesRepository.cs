﻿using System;
using System.IO;
using System.Threading.Tasks;
using Files.Common.Interfaces;
using Files.DAL.Entities;
using Files.DAL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using FileOptions = Files.Common.Options.FileOptions;

namespace Files.DAL.Repositories
{
    public class FilesRepository : IFilesRepository
    {
        private readonly FileOptions _options;

        public FilesRepository(IOptions<FileOptions> options)
        {
            _options = options.Value;
        }

        public async Task<IFileInfo> GetFileAsync(Guid id)
        {
            var fileStream = await GetFileStreamAsync(id);
            return new FileEntity
            {
                ContentType = "application/octet-stream",
                Date = DateTime.UtcNow,
                FileStream = fileStream,
                Id = id,
                IsConfirmed = true,
            };
        }

        public async Task<Guid> AddFileAsync(IFormFile file)
        {
            if (!Directory.Exists(_options.Path))
            {
                throw new NotImplementedException();
            }

            var fileId = Guid.NewGuid();
            var path = Path.Combine(_options.Path, "/", fileId.ToString()/*, Path.GetExtension(file.FileName)*/);
            
            using (var stream = File.Create(path))
            {
               await file.CopyToAsync(stream);
            }

            return fileId;
        }

        public Task RemoveUnusedFiles()
        {
            throw new NotImplementedException();
        }

        public void RemoveFile(Guid id)
        {
            var path = Path.Combine(_options.Path, "/", id.ToString());
            if (!File.Exists(path))
            {
                throw new NotImplementedException();
            }
            File.Delete(path);
        }

        private async Task<Stream> GetFileStreamAsync(Guid id)
        {
            var path = Path.Combine(_options.Path, "/", id.ToString());
            if (!File.Exists(path))
            {
                throw new NotImplementedException();
            }

            var memory = new MemoryStream();
            using (var stream = File.OpenRead(path))
            {
               await stream.CopyToAsync(memory);
            }
            return memory;
        }
    }
}
