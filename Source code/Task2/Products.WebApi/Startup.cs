﻿using Base.WebApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Products.BLL.Interfaces;
using Products.BLL.Services;
using Products.Common.Interfaces;
using Products.DAL.Context;
using Products.DAL.Interfaces;
using Products.DAL.Repositories;

namespace Products.WebApi
{
    public class Startup : BaseStartup
    {
        public Startup(IConfiguration configuration) : base(configuration)
        {
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddTransient<IResponseErrorService, ResponseErrorService>();
            services.AddDbContext<ProductsContext>(options =>
                    options.UseMySql(Configuration.GetConnectionString("MySqlConnection")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            //services.AddScoped<IUnitOfWork, MockUnitOfWork>();
            services.AddTransient<IProductsService, ProductsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            base.Configure(app, env);
        }
    }
}
