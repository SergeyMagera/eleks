﻿namespace Common.Library.Enums
{
    public enum HealthStatus
    {
        Healthy,
        Unhealthy
    }
}