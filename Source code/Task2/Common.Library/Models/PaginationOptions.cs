﻿using System.ComponentModel.DataAnnotations;

namespace Common.Library.Models
{
    public class PaginationOptions
    {
        [Range(0, int.MaxValue)]
        public int Skip { get; set; } = 0;

        [Range(1, 100)]
        public int Take { get; set; } = 20;

        public bool Ascending { get; set; }
        
        public string OrderBy() => Ascending ? "Asc" : "Desc";
    }
}
