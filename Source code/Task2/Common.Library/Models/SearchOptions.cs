﻿namespace Common.Library.Models
{
    public class SearchOptions : PaginationOptions
    {
        public string Search { get; set; } = string.Empty;
    }
}
