﻿using Common.Library.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Common.Library.Interfaces
{
    public interface IBaseResponseError
    {
        ResponseError ModelStateNotValid(ModelStateDictionary modelState);
        ResponseError InternalServerError();
    }
}