﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Library.Options
{
    public class OAuth2Options
    {
        public string Authority { get; set; }
        public bool RequireHttps { get; set; }
        public string ApiName { get; set; }
    }
}
