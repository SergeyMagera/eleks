﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Products.DAL.Interfaces;

namespace Products.DAL.Repositories
{
    public class MockUnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;
        private IProductRepository _products;

        public IProductRepository Products => _products ?? (_products = new MockProductRepository());

        public virtual void Dispose(bool disposing)
        {
            //if (!this._disposed)
            //{
            //    if (disposing)
            //    {
            //        _context.Dispose();
            //    }
            //    this._disposed = true;
            //}
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task<int> SaveAsync()
        {
            return Task.FromResult(0);
            //         return _context.SaveChangesAsync();
        }
    }
}
