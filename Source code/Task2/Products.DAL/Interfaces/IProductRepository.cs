﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.DAL.Interfaces;
using Products.Common;
using Products.Common.Interfaces;

namespace Products.DAL.Interfaces
{
    public interface IProductRepository : IBaseRepository<IProduct, int>
    {
        Task<List<IProduct>> GetByFilterAsync(ProductsOptions options);
    }
}