﻿using Base.DAL.Interfaces;

namespace Products.DAL.Interfaces
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        IProductRepository Products { get; }
    }
}