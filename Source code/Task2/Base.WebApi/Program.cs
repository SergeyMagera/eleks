﻿using Microsoft.AspNetCore.Hosting;

namespace Base.WebApi
{
    public class Program : BaseProgram
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).UseStartup<BaseStartup>().Build().Run();
        }
    }
}
