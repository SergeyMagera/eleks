﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Library.Interfaces;
using Common.Library.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Base.WebApi.Services
{
    public class BaseResponseError : IBaseResponseError
    {
        public ResponseError ModelStateNotValid(ModelStateDictionary modelState)
        {
            var errorDescriptionBuilder = new StringBuilder();
            foreach (KeyValuePair<string, ModelStateEntry> keyValuePair in modelState)
            {
                string key = keyValuePair.Key;
                ModelErrorCollection errors = keyValuePair.Value.Errors;
                if (errors != null && errors.Count > 0)
                {
                    string description = errors.Where(error => !string.IsNullOrEmpty(error.ErrorMessage))
                        .Select(error => error.ErrorMessage).Aggregate((bef, aft) => bef + aft);
                    errorDescriptionBuilder.AppendLine(string.Format(/*"Property {0} is not valid: {1}", key, */description));
                }
            }

            return new ResponseError("model_is_not_valid", errorDescriptionBuilder.ToString());
        }

        public ResponseError InternalServerError()
        {
            return new ResponseError("internal_server_error", "Critical error on the server");
        }
    }
}