﻿using System.Threading;
using System.Threading.Tasks;
using Base.WebApi.Interfaces;
using Base.WebApi.Models;
using Common.Library.Enums;

namespace Base.WebApi.Services
{
    public class DefaultHealthcheck : IHealthCheck
    {
        public Task<HealthcheckResult> Check(CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.FromResult(new HealthcheckResult(HealthStatus.Healthy, "All ok"));
        }
    }
}
