﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Base.WebApi.Builders;
using Base.WebApi.Models;
using Common.Library.Enums;
using Microsoft.AspNetCore.Http;

namespace Base.WebApi.Middlewares
{
    public class HealthcheckMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string _path;
        private readonly TimeSpan _timeout;
        public HealthcheckMiddleware(RequestDelegate next, string path, TimeSpan timeout)
        {
            _timeout = timeout;
            _path = path;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, HealthcheckBuilder healthCheckBuilder)
        {
            if (string.Equals(context.Request.Path.Value, _path, StringComparison.OrdinalIgnoreCase))
            {
                var tokenSource = new CancellationTokenSource(_timeout);
                var results = await healthCheckBuilder.GetStatus(tokenSource.Token);
                if (results.All(x => x.Status == HealthStatus.Healthy))
                {
                   await Healthy(context);
                   return;
                }

                var failedResults = results.Where(x => x.Status == HealthStatus.Unhealthy).ToList();
                await Unhealthy(context, failedResults);
            }
            else
            {
                await _next(context);
            }
        }

        async Task Healthy(HttpContext context)
        {
            context.Response.StatusCode = 200;
            context.Response.ContentLength = 2;
            await context.Response.WriteAsync("UP");
        }

        async Task Unhealthy(HttpContext context, IEnumerable<HealthcheckResult> results)
        {
            context.Response.StatusCode = 500;
            var responseDesc = results.Select(x => x.Description);
            var response = String.Join('\n', responseDesc);
            context.Response.ContentLength = response.Length;
            await context.Response.WriteAsync(response);
        }
    }
}
