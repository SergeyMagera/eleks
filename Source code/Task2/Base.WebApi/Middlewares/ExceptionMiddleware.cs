﻿using System;
using System.Threading.Tasks;
using Base.WebApi.Exceptions;
using Common.Library.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Base.WebApi.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory.CreateLogger<ExceptionMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task InvokeAsync(HttpContext context, IBaseResponseError errorService)
        {
            try
            {
                await _next(context);
            }
            catch (HttpStatusCodeException ex)
            {
                _logger.LogInformation($"Http exception was thrown:\nCode: {0}\nMessage: {0}", ex.Response.StatusCode, ex.Message);
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(ex.Response));
            }
            catch (Exception ex)
            {
                _logger.LogError("", ex);
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(errorService.InternalServerError()));
            }
        }
    }
}
