﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CheckSimilarFiles
{
    public class Program
    {
        private static readonly String Separator = new String('-', 100);
        static void Main()
        {
            var availibleDrives = Directory.GetLogicalDrives();
            var path = availibleDrives.First();
            Console.WriteLine("Avalible drives");
            foreach (var drive in availibleDrives)
            {
                Console.WriteLine(drive);
            }
            Console.WriteLine($"Current path {path}");

            while (string.Compare(path, "exit", StringComparison.OrdinalIgnoreCase) != 0)
            {
                Console.WriteLine("Please, enter path to search or exit to end");
                path = Console.ReadLine();
                if (!Directory.Exists(path))
                {
                    Console.WriteLine(string.Compare(path, "exit", StringComparison.OrdinalIgnoreCase) == 0
                        ? "Thank you for using =)"
                        : "Directory not found, please, try again");
                    continue;
                }

                CheckSimilarFiles(path).Wait();
            }
        }

        public static async Task CheckSimilarFiles(string path)
        {
            var x = new Stopwatch();
            x.Start();
            var dict = GetNestedFileNames(path, out var count)
                        .Where(xc => xc.Value.Count > 1)
                        .ToList();

            var checksTask = dict.AsParallel()
                                .Select(t => CheckFilesWithSimilarLength(t.Value))
                                .ToList();

            //await Task.WhenAll(checksTask)
            //          .ConfigureAwait(false);

            var gr = checksTask.SelectMany(r => r)
                               .Where(c => c.Count() > 1)
                               .ToList();
            x.Stop();

            foreach (var g in gr)
            {
                Console.WriteLine(GetBlockFiles(g.ToList()));
                Console.WriteLine(Separator);
            }

            Console.WriteLine($"Total files: {count} ");
            Console.WriteLine($"Time for searching: {x.Elapsed}");
        }

        public static List<IGrouping<string, string>> CheckFilesWithSimilarLength(List<string> files)
        {
            var sim = files
                .GroupBy(GetHashCode)
                .ToList();
            return sim;
        }

        public static string GetBlockFiles(List<string> files)
        {
            return files.Aggregate((prev, cur) => string.Concat(prev, "\n", cur));
        }

        public static Dictionary<long, List<string>> GetNestedFileNames(string path, out int count)
        {
            var dict = new Dictionary<long, List<string>>();
            try
            {
                var fileNames = Directory
                    .GetFiles(path, "*.*", new EnumerationOptions
                    {
                        IgnoreInaccessible = true,
                        RecurseSubdirectories = true,
                        ReturnSpecialDirectories = false,
                        AttributesToSkip = FileAttributes.System | FileAttributes.Hidden | FileAttributes.Temporary
                    });
                count = fileNames.Length;
                dict = fileNames
                    .GroupBy(x => new FileInfo(x).Length)
                    .ToDictionary(ks => ks.Key, v => v.ToList());
            }
            catch (Exception)
            {
                count = 0;
                // ignored
            }

            return dict;
        }

        public static string GetHashCode(string filename)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        var hash = md5.ComputeHash(stream);
                        return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                    }
                }
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }
    }
}
